Esempio di utilizzo:

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    //------------
    // l'assegnazione ad una variabile è opzionale al fatto se si voglia o meno gestire il permessi anche in onRequestPermissionsResult
    private var singlePermissionsCustomUtil: PermissionUtility.PermissionsCustomUtil? = null  // opzionale
    private var multiPermissionsCustomUtil: PermissionUtility.PermissionsCustomUtil? = null  // opzionale
    //------------

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.button1permission.setOnClickListener {

            singlePermissionsCustomUtil = requestPermissionsCustom(
                arrayOf(Manifest.permission.CAMERA),
                onAllPermissionGranted = { binding.textView.text = "Permesso garantito" },
                onNeedToExplain = { missingPermissions: ArrayList<String>, shouldGoToSettingApp: Boolean, openSetting: () -> Unit, repeatRequest: () -> Unit ->

                    // Usare questo metodo per visualizzare un popup di spiegazione (molto semplice) con delle logiche gia configurate.
                    // Oppure creare un popup adhoc
                    PermissionUtility.createSimpleAlert(
                        this,
                        "Richiesta permesso",
                        "Questi permesso ${
                            missingPermissions.toString().replace("android.permission.", "")
                        } è neccessario per il corretto funzionamento dell'app",
                        shouldGoToSettingApp, openSetting, repeatRequest
                    )
                },
                124
            )
        }

        binding.button2permission.setOnClickListener {
            multiPermissionsCustomUtil = requestPermissionsCustom(
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CALENDAR),
                onAllPermissionGranted = { binding.textView2.text = "Permessi garantiti" },
                onNeedToExplain = { missingPermissions: ArrayList<String>, shouldGoToSettingApp: Boolean, openSetting: () -> Unit, repeatRequest: () -> Unit ->
                    AlertDialog.Builder(this)
                        .setTitle("Richiesta permessi")
                        .setMessage("Questi permessi ${PermissionUtility.convertPermissionToHuman(missingPermissions)} sono neccessari per il corretto funzionamento dell'app")
                        .setPositiveButton(getString(android.R.string.ok)) { _, _ ->
                            if (shouldGoToSettingApp) {
                                // anche la seconda volta è stata chiesta, ma rifiutata, adesso lo porto nelle impostazioni
                                openSetting()
                            } else {
                                // richiedo per la seconda volta
                                repeatRequest()
                            }
                        }
                        .setNegativeButton(getString(android.R.string.cancel)) { dialog, _ -> dialog?.dismiss() }
                        .setCancelable(false)
                        .create().show()
                },
                123
            )
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            singlePermissionsCustomUtil?.requestCode -> {
                if (singlePermissionsCustomUtil?.isAllPermissionsGrant(grantResults) == true) {
                    singlePermissionsCustomUtil?.doAction()
                }
            }
            multiPermissionsCustomUtil?.requestCode -> {
                if (multiPermissionsCustomUtil?.isAllPermissionsGrant(grantResults) == true) {
                    multiPermissionsCustomUtil?.doAction()
                }
            }
        }
    }

}
