package com.jonlab.runtimepermissionutiljon

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.jonlab.runtimepermissionutiljon.PermissionUtility.requestPermissionsCustom

/**
 * Created by Jonathan on 02/02/2021.
 */
object PermissionUtility {

    private val TAG = "PermissionUtility"
    private val nameLibrayForSharedPref = "RuntimePermissionUtilJon"

    /***
     * Helper per la richiesta singola o multipla dei permessi
     *
     * Esempio di utilizzo:
     *
     * multiPermissionsCustomUtil = requestPermissionsCustom(
    arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CALENDAR),
    onAllPermissionGranted = { binding.textView2.text = "Permessi garantiti" },
    onNeedToExplain = { missingPermissions: ArrayList<String>, shouldGoToSettingApp: Boolean, openSetting: () -> Unit, repeatRequest: () -> Unit ->
    AlertDialog.Builder(this)
    .setTitle("Richiesta permessi")
    .setMessage("Questi permessi ${PermissionUtility.convertPermissionToHuman(missingPermissions)} sono neccessari per il corretto funzionamento dell'app")
    .setPositiveButton(getString(android.R.string.ok)) { _, _ ->
    if (shouldGoToSettingApp) {
    // anche la seconda volta è stata chiesta, ma rifiutata, adesso lo porto nelle impostazioni
    openSetting()
    } else {
    // richiedo per la seconda volta
    repeatRequest()
    }
    }
    .setNegativeButton(getString(android.R.string.cancel)) { dialog, _ -> dialog?.dismiss() }
    .setCancelable(false)
    .create().show()
    },
    123
    )
     *
     * @param permissions array dei permessi da richiedere
     * @param onAllPermissionGranted passare il codice/funzione da eseguire quando tutti i permessi sono stati consentiti
     * @param onNeedToExplain passare il codice/funzione da eseguire quando almeno uno dei permessi richiesti è stato declinato piu di 2 volte, inoltre il metodo fornisce dei parametri di utilità e check
     * @param requestCode codice per la richiesta dei permessi, utile da verificare in [Activity.onRequestPermissionsResult]
     * @return opzionalmente ritorna l'oggetto PermissionsCustomUtil di utilità in [Activity.onRequestPermissionsResult]
     */
    fun Activity.requestPermissionsCustom(
        permissions: Array<String>,
        onAllPermissionGranted: () -> Unit,
        onNeedToExplain: (ArrayList<String>, openSetting: () -> Unit) -> Unit,
        requestCode: Int
    ): PermissionsCustomUtil? {
        checkPermissionInManifest(this, permissions)
        val missingPermissions = arrayListOf<String>()
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                missingPermissions.add(permission)
            } else {
                resetCountPermission(this, permission)
            }
        }
        if (missingPermissions.isEmpty()) {
            // tutti i permessi sono accettati
            onAllPermissionGranted()
        } else {
            val permissionsToRequest = checkIssueBackgroundLocation(missingPermissions)
            if (shouldShowExplain(this, permissionsToRequest)) {
                // è la terza richiesta, devo spiegare il motivo del permesso
                onNeedToExplain(
                    missingPermissions,
                    { openApplicationDetailSetting(this) })
            } else {
                // prima e seconda richiesta, non mostro nessun popup e procedo con la richiesta
                startRequestPermission(this, permissionsToRequest, requestCode)
            }
            return PermissionsCustomUtil(missingPermissions, onAllPermissionGranted, requestCode)
        }
        return null
    }

    private fun resetCountPermission(activity: Activity, permission: String) {
        val sharedPref = activity.getSharedPreferences(nameLibrayForSharedPref, Context.MODE_PRIVATE)
        sharedPref.edit()
            .putInt("Asked-${permission}", 1) // imposto 1 per bypassare il fatto che un utente possa premere su "Non chiedere ancora"
            .apply()
    }

    private fun startRequestPermission(activity: Activity, permissionToRequest: ArrayList<String>, requestCode: Int) {
        ActivityCompat.requestPermissions(activity, permissionToRequest.toTypedArray(), requestCode).also {
            for (permission in permissionToRequest) {
                val sharedPref = activity.getSharedPreferences(nameLibrayForSharedPref, Context.MODE_PRIVATE)
                sharedPref.edit()
                    .putInt("Asked-${permission}", sharedPref.getInt("Asked-${permission}", 0).plus(1))
                    .apply()
            }
        }
    }

    private fun shouldShowExplain(activity: Activity, permissions: ArrayList<String>): Boolean {
        for (permission in permissions) {
            val result = activity.getSharedPreferences(nameLibrayForSharedPref, Context.MODE_PRIVATE)
                .getInt("Asked-${permission}", 0) > 1
            if (result) return true
        }
        return false
    }

    private fun openApplicationDetailSetting(activity: Activity) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.data = Uri.fromParts("package", activity.packageName, null)
        activity.startActivity(intent)
    }

    /**
     * Controlla se almeno uno dei permessi richiesti non è dichiarato nel manifest e nel caso lancia una eccezione!!!
     */
    private fun checkPermissionInManifest(context: Activity, permissions: Array<String>) {
        val info: PackageInfo = context.packageManager.getPackageInfo(context.packageName, PackageManager.GET_PERMISSIONS)
        val permissionsInManifest = info.requestedPermissions
        if (permissionsInManifest != null) {
            for (permission in permissions) {
                if (!permissionsInManifest.contains(permission)) {
                    throw Exception("--->> NEL MANIFEST NON E' PRESENTE QUESTO PERMESSO: $permission <<---")
                }
            }
        } else {
            throw Exception("--->> NEL MANIFEST NON E' PRESENTE ALCUN PERMESSO!\nPERMESSI RICHIESTI FINORA: ${permissions.contentToString()} <<---")
        }

    }

    /**
     * Workaround per gestire la nuova logica per la richiesta del permesso di localizzazione in BACKGROUND.
     * Se trova che nella lista dei permessi da richiedere sono presenti assieme ACCESS_FINE_LOCATION & ACCESS_BACKGROUND_LOCATION
     * oppure ACCESS_COARSE_LOCATION & ACCESS_BACKGROUND_LOCATION, rimuove ACCESS_BACKGROUND_LOCATION in modo da poter gestire la richiesta di
     * ACCESS_BACKGROUND_LOCATION nella callback [Activity.onRequestPermissionsResult].
     * PS.: al seconda richiesta andrebbe eseguita dopo aver mostrato un popup informativo.
     */
    private fun checkIssueBackgroundLocation(missingPermissions: ArrayList<String>): ArrayList<String> {
        val permissionsToRequest = arrayListOf<String>()
        permissionsToRequest.addAll(missingPermissions)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            if ((missingPermissions.contains(Manifest.permission.ACCESS_FINE_LOCATION) && missingPermissions.contains(Manifest.permission.ACCESS_BACKGROUND_LOCATION)) ||
                (missingPermissions.contains(Manifest.permission.ACCESS_COARSE_LOCATION) && missingPermissions.contains(Manifest.permission.ACCESS_BACKGROUND_LOCATION))
            ) {
                permissionsToRequest.remove(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                Log.e(
                    TAG, "\n--->> ATTENZIONE, DA ANDROID 11+ IL PERMESSO ${Manifest.permission.ACCESS_BACKGROUND_LOCATION}\n" +
                            "NON PUO' ESSERE RICHIESTO ASSIEME A ${Manifest.permission.ACCESS_FINE_LOCATION} E ${Manifest.permission.ACCESS_COARSE_LOCATION}.\n" +
                            "RICHIEDERE ${Manifest.permission.ACCESS_BACKGROUND_LOCATION} SUCCESSIVAMENTE ALLA CONSESSIONE DEI 2 PERMESSI PRECEDENTI.\n" +
                            "reference: https://developer.android.com/about/versions/11/privacy/location#background-location <<---"
                )
            }
        }
        return permissionsToRequest
    }

    /***
     * Metodo utility per creare e visualizzare in velocità un popup per la spiegazione di cosa servono i permessi richiesti.
     * Come da "best practice".
     * Utilizzare quindi anche come esempio per un popup creato adhoc.
     */
    fun createSimpleAlert(
        context: Activity,
        title: String,
        message: String,
        openSetting: () -> Unit
    ) {
        (AlertDialog.Builder(context)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(context.getString(android.R.string.ok)) { _, _ ->
                // anche la seconda volta è stata chiesta, ma rifiutata, adesso lo porto nelle impostazioni
                openSetting()
            }
            .setNegativeButton(context.getString(android.R.string.cancel)) { dialog, _ -> dialog?.dismiss() }
            .setCancelable(false)
            .create()
            .show())
    }

    /***
     * Converte i permessi del manifest in qualcosa di leggibile per l'utente finale e che verosimilmente troverà nelle impostazione dell'app come autorizzazioni.
     *
     * TODO: da aggiornare con eventuali nuovi/mancanti i permessi
     */
    fun convertPermissionToHuman(missingPermissions: ArrayList<String>): List<String> {
        return missingPermissions.map {
            when (it) {
                Manifest.permission.CAMERA -> {
                    "Fotocamera"
                }
                Manifest.permission.ACCESS_FINE_LOCATION -> {
                    "Geolocalizzazione"
                }
                Manifest.permission.ACCESS_COARSE_LOCATION -> {
                    "Geolocalizzazione"
                }
                Manifest.permission.ACCESS_BACKGROUND_LOCATION -> {
                    "Geolocalizzazione"
                }
                Manifest.permission.WRITE_EXTERNAL_STORAGE -> {
                    "File e contenuti multimediali"
                }
                Manifest.permission.READ_EXTERNAL_STORAGE -> {
                    "File e contenuti multimediali"
                }
                Manifest.permission.ACCESS_MEDIA_LOCATION -> {
                    "File e contenuti multimediali"
                }
                Manifest.permission.READ_CALENDAR -> {
                    "Calendario"
                }
                Manifest.permission.WRITE_CALENDAR -> {
                    "Calendario"
                }
                Manifest.permission.READ_CONTACTS -> {
                    "Contatti"
                }
                Manifest.permission.WRITE_CONTACTS -> {
                    "Contatti"
                }
                Manifest.permission.GET_ACCOUNTS -> {
                    "Contatti"
                }
                Manifest.permission.RECORD_AUDIO -> {
                    "Microfono"
                }
                Manifest.permission.READ_PHONE_STATE -> {
                    "Telefono"
                }
                Manifest.permission.READ_PHONE_NUMBERS -> {
                    "Telefono"
                }
                Manifest.permission.CALL_PHONE -> {
                    "Telefono"
                }
                Manifest.permission.ANSWER_PHONE_CALLS -> {
                    "Telefono"
                }
                Manifest.permission.ADD_VOICEMAIL -> {
                    "Telefono"
                }
                Manifest.permission.USE_SIP -> {
                    "Telefono"
                }
                Manifest.permission.ACCEPT_HANDOVER -> {
                    "Telefono"
                }
                Manifest.permission.READ_CALL_LOG -> {
                    "Registri chiamate"
                }
                Manifest.permission.WRITE_CALL_LOG -> {
                    "Registri chiamate"
                }
                Manifest.permission.SEND_SMS -> {
                    "SMS"
                }
                Manifest.permission.RECEIVE_SMS -> {
                    "SMS"
                }
                Manifest.permission.READ_SMS -> {
                    "SMS"
                }
                Manifest.permission.RECEIVE_WAP_PUSH -> {
                    "SMS"
                }
                Manifest.permission.RECEIVE_MMS -> {
                    "SMS"
                }
                Manifest.permission.BODY_SENSORS -> {
                    "Attività fisica"
                }
                Manifest.permission.ACTIVITY_RECOGNITION -> {
                    "Attività fisica"
                }
                else -> {
                    it.replace("android.permission.", "")
                }
            }
        }.distinct()
    }

    class PermissionsCustomUtil(val missingPermissions: ArrayList<String>, private val onAllPermissionGranted: () -> Unit, val requestCode: Int) {

        fun getGrantedPermissions(activity: Activity): ArrayList<String> {
            val grantedPermissions = arrayListOf<String>()
            for (permission in missingPermissions) {
                if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED) {
                    grantedPermissions.add(permission)
                }
            }
            return grantedPermissions
        }

        fun getDeniedPermissions(activity: Activity): ArrayList<String> {
            val deniedPermissions = arrayListOf<String>()
            for (permission in missingPermissions) {
                if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_DENIED) {
                    deniedPermissions.add(permission)
                }
            }
            return deniedPermissions
        }

        fun isAllPermissionsGrant(grantResults: IntArray): Boolean {
            return missingPermissions.size == grantResults.size && !grantResults.contains(PackageManager.PERMISSION_DENIED)
        }

        fun doGrantedAction() {
            onAllPermissionGranted()
        }
    }
}